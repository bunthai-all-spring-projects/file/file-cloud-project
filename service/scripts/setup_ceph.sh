#!/usr/bin/env bash
#add param ceph.conf configuration
docker-compose -f docker-compose.ceph.yml exec ceph-mon bash -c "echo \"osd pool default size = 2\" >> /etc/ceph/ceph.conf"
docker-compose -f docker-compose.ceph.yml exec ceph-mon bash -c "echo \"osd pool default min_size = 2\" >> /etc/ceph/ceph.conf"
docker-compose -f docker-compose.ceph.yml exec ceph-mon bash -c "echo \"osd max object name len = 256\" >> /etc/ceph/ceph.conf"
docker-compose -f docker-compose.ceph.yml exec ceph-mon bash -c "echo \"osd max object namespace len = 64\" >> /etc/ceph/ceph.conf"
docker-compose -f docker-compose.ceph.yml exec ceph-mon bash -c "echo \"[osd.0]\" >> /etc/ceph/ceph.conf"
docker-compose -f docker-compose.ceph.yml exec ceph-mon bash -c "echo \"journal aio = true\" >> /etc/ceph/ceph.conf"
docker-compose -f docker-compose.ceph.yml exec ceph-mon bash -c "echo \"journal dio = true\" >> /etc/ceph/ceph.conf"
docker-compose -f docker-compose.ceph.yml exec ceph-mon bash -c "echo \"journal block align = true\" >> /etc/ceph/ceph.conf"
docker-compose -f docker-compose.ceph.yml exec ceph-mon bash -c "echo \"journal force aio = true\" >> /etc/ceph/ceph.conf"
docker-compose -f docker-compose.ceph.yml exec ceph-mon bash -c "echo \"[client]\" >> /etc/ceph/ceph.conf"
docker-compose -f docker-compose.ceph.yml exec ceph-mon bash -c 'echo rgw frontends=\"civetweb port=8080\" >> /etc/ceph/ceph.conf'

#restart ceph again
docker-compose -f docker-compose.ceph.yml restart
docker-compose -f docker-compose.ceph.yml up -d

#create ceph an admin user
docker-compose -f docker-compose.ceph.yml exec rados-gateway \
radosgw-admin user create --uid=admin --display-name=admin --access-key filecloud --secret-key filecloud --caps="users=write"
