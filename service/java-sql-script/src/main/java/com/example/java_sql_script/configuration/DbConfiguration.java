package com.example.java_sql_script.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
public class DbConfiguration {

    Logger log = LoggerFactory.getLogger(DbConfiguration.class);

    @Value("${spring.datasource.url}")
    String url;

    @Value("${spring.datasource.driver-class-name}")
    String driver;

    @Value("${spring.datasource.username}")
    String username;

    @Bean
    public DriverManagerDataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(username);

        try {
            // db init
            Resource initDb = new ClassPathResource("scripts/1_create_db.sql");
            /**
             * it can be specified multiple scripts in ResourceDatabasePopulator(...)
             */
            DatabasePopulator databasePopulator = new ResourceDatabasePopulator(initDb);
            DatabasePopulatorUtils.execute(databasePopulator, dataSource);
            log.info("Executed file: " + initDb.getFilename());
            return dataSource;
        } catch (Exception e){
            log.info(e.getMessage());
            return dataSource;
        }
    }


    @Bean
    public DriverManagerDataSource dataSource2() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url+"node");
        dataSource.setUsername(username);

        try {

            List<Resource> resources = Arrays.asList(
                "scripts/2_create_table.sql"
            ).stream().map(path -> new ClassPathResource(path)).collect(Collectors.toList());

            //schema init
            /**
             * it can be specified multiple scripts in ResourceDatabasePopulator(...)
             */
            DatabasePopulator databasePopulator = new ResourceDatabasePopulator(resources.stream().toArray(Resource[]::new));
            DatabasePopulatorUtils.execute(databasePopulator, dataSource);
            log.info("Executed file: " + resources.stream().map(r -> r.getFilename()).collect(Collectors.joining(", ")));

            return dataSource;
        } catch (Exception e) {
            log.info(e.getMessage());
            return dataSource;
        }
    }

}
