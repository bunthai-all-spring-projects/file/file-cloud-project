CREATE SEQUENCE file_info_id_seq;
CREATE TABLE file_info (
   id BIGINT PRIMARY KEY DEFAULT NEXTVAL('file_info_id_seq'),
   file_key VARCHAR(100) UNIQUE,
   file_name VARCHAR(255),
   file_size BIGINT,
   created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
);