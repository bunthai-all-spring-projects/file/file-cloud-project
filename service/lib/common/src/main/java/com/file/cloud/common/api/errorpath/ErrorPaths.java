package com.file.cloud.common.api.errorpath;

import lombok.NonNull;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ErrorPaths
{
	public static ErrorPath header(@NonNull String _name)
	{
		return new ErrorPath(ErrorPathType.HEADER, _name);
	}

	public static ErrorPath query(@NonNull String _name)
	{
		return new ErrorPath(ErrorPathType.QUERY, _name);
	}

	public static ErrorPath path(@NonNull String _name)
	{
		return new ErrorPath(ErrorPathType.PATH, _name);
	}

	public static ErrorPath body(@NonNull String _name)
	{
		return new ErrorPath(ErrorPathType.BODY, _name);
	}

	public static ErrorPath unhandled()
	{
		return new ErrorPath(ErrorPathType.UNHANDLED, "unhandled");
	}
}
