package com.file.cloud.common.api.responses;

import com.file.cloud.common.api.errorpath.ErrorPath;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiError {
    private HttpStatus httpStatus;
    private  String message;
    private ErrorPath errorPath;
}
