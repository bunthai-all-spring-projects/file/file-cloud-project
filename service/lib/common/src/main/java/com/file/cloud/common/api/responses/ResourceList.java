package com.file.cloud.common.api.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

//@Builder(builderClassName = "Builder")
@Data
@AllArgsConstructor
public class ResourceList<T> {
    private List<T> items;
    private int page;
    private int size;
    private int countItems;
    private int countPages;
    private long countAll;

}
