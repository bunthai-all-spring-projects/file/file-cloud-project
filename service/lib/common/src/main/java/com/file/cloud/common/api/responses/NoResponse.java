package com.file.cloud.common.api.responses;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class NoResponse extends ResponseEntity {
    public NoResponse() {
        super(HttpStatus.OK);
    }
}
