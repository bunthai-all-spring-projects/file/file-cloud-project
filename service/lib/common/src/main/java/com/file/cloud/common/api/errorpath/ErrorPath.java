package com.file.cloud.common.api.errorpath;

import lombok.Getter;
import lombok.NonNull;

public class ErrorPath
{
	@Getter
	private ErrorPathType type;

	@Getter
	@NonNull
	private String name;

	protected ErrorPath(@NonNull ErrorPathType _type, @NonNull String _name)
	{
		this.type = _type;
		this.name = _name;
	}

}
