package com.file.cloud.common.api.responses;

import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


public class ResourceNotFoundResponse extends ResponseEntity<Payload>
{
	public ResourceNotFoundResponse(@NonNull ApiError _error)
	{
		super(new Payload(HttpStatus.NOT_FOUND.value(), "not found", _error), _error.getHttpStatus());
	}
}
