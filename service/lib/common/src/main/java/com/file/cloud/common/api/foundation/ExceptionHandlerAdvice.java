package com.file.cloud.common.api.foundation;

import antlr.StringUtils;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.file.cloud.common.api.errorpath.ErrorPaths;
import com.file.cloud.common.api.responses.ApiError;
import com.file.cloud.common.api.responses.ResourceNotFoundResponse;
import com.file.cloud.common.api.responses.ResourceViolationResponse;
import com.file.cloud.common.exceptions.ErrorResponse;
import com.file.cloud.common.exceptions.ResourceNotFoundException;
import com.file.cloud.common.exceptions.ResourceViolationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.IOException;
import java.util.List;


@Slf4j
@RestControllerAdvice
public class ExceptionHandlerAdvice {


    /**
     * for @NonNull
     */
    @ExceptionHandler
    public ResponseEntity formNullValue(JsonMappingException _e) {

//        log.info("ssss: {}, {}, {}, {},,, {}", _e.getMessage(), _e.getProcessor(), _e.getPathReference(), _e.getPath(), _e.getOriginalMessage());
        String errorPath = StringUtils.stripFrontBack(_e.getPathReference(), "\"", "\"");
        String message = _e.getOriginalMessage();
        if(message.contains("is marked non-null but is null"))
            message = String.format("must be not null");
        log.info("Error: {}, {} {}", HttpStatus.BAD_REQUEST, errorPath, message);
        return new ResourceViolationResponse(new ApiError(HttpStatus.BAD_REQUEST, message, ErrorPaths.body(errorPath)));
    }

    /**
     * for @NotNull, Pattern, Empty String
     */
    @ExceptionHandler
    public ResponseEntity form(MethodArgumentNotValidException _e) {

        FieldError fieldError = _e.getBindingResult().getFieldError(); //.getFieldErrors().get(fieldErrors.size() - 1);
            log.info("Error: {}, {} {}", HttpStatus.BAD_REQUEST, fieldError.getField(), (fieldError.getDefaultMessage()));
        String message = fieldError.getDefaultMessage();
//        log.info("dddddd {} \n {} \n {}", _e.getBindingResult().getPropertyEditorRegistry());
        return new ResourceViolationResponse(new ApiError(HttpStatus.BAD_REQUEST, message, ErrorPaths.body(fieldError.getField())));
    }

    @ExceptionHandler
    public ResponseEntity notFound(ResourceNotFoundException _e) {

        log.info("Error: {}, {}", HttpStatus.NOT_FOUND, _e.getMessage());
        if (_e.getError().isPresent()) {
            return new ResourceNotFoundResponse(_e.getError().get());
        }
        return new ErrorResponse(HttpStatus.NOT_FOUND, "not found");
    }


    @ExceptionHandler
    public ResponseEntity violation(ResourceViolationException _e) {

        log.info("Error: {}, {}", HttpStatus.BAD_REQUEST, _e.getMessage());
        if (_e.getError().isPresent()) {
            return new ResourceViolationResponse(_e.getError().get());
        }
        return new ErrorResponse(HttpStatus.BAD_REQUEST, "violation");
    }

    @ExceptionHandler
    public ResponseEntity exception(Exception _e) throws Exception {
        log.info("Error: {}, {}", HttpStatus.BAD_REQUEST, _e.getMessage());
        if (_e instanceof IllegalArgumentException ) {
            return new ResourceViolationResponse(new ApiError(HttpStatus.BAD_REQUEST, _e.getMessage(), ErrorPaths.unhandled()));
        } else if(_e instanceof MissingServletRequestParameterException) {
            String param = ((MissingServletRequestParameterException)_e).getParameterName();
            return new ResourceViolationResponse(new ApiError(HttpStatus.BAD_REQUEST, _e.getMessage(), ErrorPaths.query(param)));
        }
        throw new Exception(_e.getMessage());
    }

}
