package com.file.cloud.common.exceptions;

import com.file.cloud.common.api.responses.ApiError;

import java.util.Optional;

public class ResourceException extends Exception
{
    private ApiError error;

    public ResourceException(String _message, ApiError _error)
    {
        super(_message);
        error = _error;
    }

    public Optional<ApiError> getError()
    {
        return Optional.ofNullable(this.error);
    }
}
