package com.file.cloud.common.infrastructure.configuration;

import com.file.cloud.common.api.foundation.ExceptionHandlerAdvice;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ConfigurationProperties
@Import(ExceptionHandlerAdvice.class)
public class ApiConfiguration {
}
