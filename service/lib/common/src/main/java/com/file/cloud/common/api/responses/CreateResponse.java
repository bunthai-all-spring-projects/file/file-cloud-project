package com.file.cloud.common.api.responses;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class CreateResponse<T> extends ResponseEntity<T> {
    public CreateResponse(T body) {
        super(body, HttpStatus.CREATED);
    }
}
