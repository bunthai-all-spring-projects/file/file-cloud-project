package com.file.cloud.common.exceptions;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ErrorResponse extends ResponseEntity<ErrorResponse.Payload>
{
	public ErrorResponse(@NonNull HttpStatus _status, @NonNull String _message)
	{
		super(new Payload(_status.value(), _message), _status);
	}

	@Value
	@AllArgsConstructor
	@EqualsAndHashCode
	public static class Payload
	{
		@NonNull
		private Integer status;

		@NonNull
		private String message;
	}
}


