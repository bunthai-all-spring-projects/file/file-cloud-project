package com.file.cloud.common.exceptions;

import com.file.cloud.common.api.responses.ApiError;

public class ResourceViolationException extends ResourceException
{
    public ResourceViolationException(String _message, ApiError _error)
    {
        super(_message, _error);
    }
}
