package com.file.cloud.common;

import com.file.cloud.common.api.errorpath.ErrorPaths;
import com.file.cloud.common.api.responses.ApiError;
import com.file.cloud.common.exceptions.ResourceNotFoundException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
//@RestController
public class CommonApplication { //implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(CommonApplication.class, args);
	}

//	@Override
//	public void run(String... args) throws Exception {
//		//throw new Exception("Test");
////		throw new ResourceException("resource");
//	}
//
//	@GetMapping
//	public String test() throws Exception {
//
//		if (true)
//			throw new ResourceNotFoundException("not found", new ApiError(HttpStatus.NOT_FOUND, "resource not found", ErrorPaths.body("ids")));
//
//		return "a";
//	}

}
