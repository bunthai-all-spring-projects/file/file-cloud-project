package com.file.cloud.common.api.responses;

import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


public class ResourceViolationResponse extends ResponseEntity<Payload>
{
	public ResourceViolationResponse(@NonNull ApiError _error)
	{
		super(new Payload(HttpStatus.BAD_REQUEST.value(), "violation", _error), _error.getHttpStatus());
	}
}
