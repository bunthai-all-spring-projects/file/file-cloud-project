package com.file.cloud.common.api.responses;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class ListResponse<T> extends ResponseEntity<ResourceList<T>> {
    public ListResponse(List<T> body, int page, int size, int countItems, int countPages, long countAll) {
        super(new ResourceList<>(body, page, size, countItems, countPages, countAll), HttpStatus.OK);
    }
}
