package com.file.cloud.common.api.responses;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Value;

@Value
@AllArgsConstructor
@EqualsAndHashCode
public class Payload
{
    @NonNull
    private Integer status;
    @NonNull
    private String message;
    @NonNull
    private ApiError error;

}