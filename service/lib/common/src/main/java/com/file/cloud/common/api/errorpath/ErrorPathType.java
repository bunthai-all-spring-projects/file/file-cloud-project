package com.file.cloud.common.api.errorpath;

public enum ErrorPathType
{
	QUERY,
	HEADER,
	BODY,
	PATH,
	UNHANDLED;

	@Override
	public String toString()
	{
		return this.name().toLowerCase();
	}
}
