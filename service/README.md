To Exexcute project

// Create connection
> $ docker network create --gateway 172.18.0.1 --subnet 172.18.0.0/16 test

// run ceph-mon
> $ docker-compose -f docker-compose.ceph.yml up -d ceph-mon

// run Postgres
> $ docker-compose up -d postgresql

//run ceph
$ ./scripts/setup_ceph.sh
> ./gradlew build && java -jar build/libs/java_sql_script-0.0.1-SNAPSHOT.jar

