package com.file.cloud.fileservice.domain.entities;

import com.file.cloud.fileservice.domain.entities.objectvalues.FileKey;
import com.file.cloud.fileservice.domain.entities.objectvalues.FileName;
import com.file.cloud.fileservice.domain.entities.objectvalues.FileSize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.OffsetDateTime;

@Data
@Entity
@Builder(builderClassName = "Builder")
@AllArgsConstructor
@NoArgsConstructor
//@IdClass(Id.class)
public class FileInfo {

    @Id
    @SequenceGenerator(name = "file_info_seq", sequenceName = "file_info_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "file_info_seq")
    private long id;

//    @EmbeddedId
//    @Transient
//    private Id id;
    private FileName fileName;
    private FileKey fileKey;
    private FileSize fileSize;

//    @Transient

    @Column(insertable = false, name = "created_at")
    private OffsetDateTime createdAt;

//    @Id
//    @SequenceGenerator(name = "file_info_seq", sequenceName = "file_info_id_seq", allocationSize = 1)
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "file_info_seq")
//    private long id;
//    private String fileKey;
//    private String fileName;
//    private long fileSize;


//    @PostPersist
//    @PostLoad
//    private void setupId()
//    {
//        this.id = Id.builder().value(this.tableId).build();
//    }

//    @Converter(autoApply = true)
//    public static class JpaConverter implements AttributeConverter<OffsetDateTime, Timestamp>
//    {
//        @Override
//        public Timestamp convertToDatabaseColumn(OffsetDateTime _attribute)
//        {
//            return Timestamp.from(_attribute.toInstant());
//        }
//
//        @Override
//        public OffsetDateTime convertToEntityAttribute(Timestamp _dbData)
//        {
//            return OffsetDateTime.from(_dbData.toInstant());
//        }
//    }

}
