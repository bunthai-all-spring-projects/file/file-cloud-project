package com.file.cloud.fileservice.domain.repository;

import com.file.cloud.common.exceptions.ResourceNotFoundException;
import com.file.cloud.fileservice.domain.entities.FileInfo;
import com.file.cloud.fileservice.domain.entities.objectvalues.FileKey;

import java.util.List;
import java.util.Optional;

public interface FileInfoRepository {
    void deleteFileInfoByFileKey(FileKey fileKey) throws ResourceNotFoundException;

    Optional<FileInfo> getFileInfoByFileKey(FileKey fileKey);
    List<FileInfo> getFileInfoes();
    
}
