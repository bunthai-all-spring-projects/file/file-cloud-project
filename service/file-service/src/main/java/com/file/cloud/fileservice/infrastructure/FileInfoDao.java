package com.file.cloud.fileservice.infrastructure;

import com.file.cloud.common.api.errorpath.ErrorPaths;
import com.file.cloud.common.api.responses.ApiError;
import com.file.cloud.common.exceptions.ResourceNotFoundException;
import com.file.cloud.common.exceptions.ResourceViolationException;
import com.file.cloud.fileservice.domain.entities.FileInfo;
import com.file.cloud.fileservice.domain.entities.objectvalues.FileKey;
import com.file.cloud.fileservice.domain.entities.objectvalues.FileName;
import com.file.cloud.fileservice.domain.entities.objectvalues.FileSize;
import com.file.cloud.fileservice.domain.repository.FileInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

@Repository
public class FileInfoDao implements FileInfoRepository {


    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public void deleteFileInfoByFileKey(FileKey fileKey) throws ResourceNotFoundException {
        String sql = "DELETE FROM file_info WHERE file_key = :fileKey";
        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue("fileKey", fileKey.getValue());
        if(jdbcTemplate.update(sql, param) == 0) {
            throw new ResourceNotFoundException("not found", new ApiError(HttpStatus.NOT_FOUND, "fileinfo not found",  ErrorPaths.path("fileKey")));
        }
    }
    
    @Override
    public Optional<FileInfo> getFileInfoByFileKey(FileKey fileKey) {

        String sql = "SELECT * FROM file_info WHERE file_key = :fileKey";
        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue("fileKey", fileKey.getValue());
        try {
            FileInfo fileInfo = jdbcTemplate.queryForObject(sql, param, (rs, rowNum) -> {
                System.out.println("timeeeeeeeeee: " +rs.getTimestamp("created_at") + "; \n zoneidddd: " + ZoneId.systemDefault());
                return FileInfo.builder()
                        .fileKey(FileKey.builder().value(rs.getString("file_key")).build())
                        .fileName(FileName.builder().value(rs.getString("file_name")).build())
                        .fileSize(FileSize.builder().value(rs.getLong("file_size")).build())
//                        .createdAt(OffsetDateTime.from(rs.getTimestamp("created_at").toInstant()))
                        .createdAt(OffsetDateTime.ofInstant(rs.getTimestamp("created_at").toInstant(), ZoneId.systemDefault()))
                        .build();
            });
            return Optional.of(fileInfo);
        } catch (DataAccessException e){
            return Optional.empty();
        }
    }

    @Override
    public List<FileInfo> getFileInfoes() {
        return null;
    }
}
