package com.file.cloud.fileservice.domain.entities.objectvalues;

import lombok.Builder;
import lombok.Value;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import javax.validation.constraints.NotNull;

@Value
@Builder(builderClassName = "Builder")
public class FileKey {

    @NotNull
    private String value;


    @Converter(autoApply = true)
    public static class JpaConverter implements AttributeConverter<FileKey, String>
    {
        @Override
        public String convertToDatabaseColumn(FileKey _attribute)
        {
            return _attribute.value;
        }

        @Override
        public FileKey convertToEntityAttribute(String _dbData)
        {
            return FileKey.builder().value(_dbData).build();
        }
    }

}
