package com.file.cloud.fileservice.api.view;

import com.file.cloud.fileservice.domain.entities.FileInfo;
import lombok.Builder;
import lombok.Value;

@Value
public class FileInfoView {
    private String fileKey;
    private String fileName;
    private long fileSize;
    private String createdAt;

//    public FileInfoView(FileInfo fileInfo) {
//        this.fileKey = fileInfo.getFileKey();
//        this.fileName = fileInfo.getFileName();
//        this.fileSize = fileInfo.getFileSize();
//    }

    public FileInfoView(FileInfo fileInfo) {
        this.fileKey = fileInfo.getFileKey().getValue();
        this.fileName = fileInfo.getFileName().getValue();
        this.fileSize = fileInfo.getFileSize().getValue();
        this.createdAt = String.valueOf(fileInfo.getCreatedAt());
    }
}
