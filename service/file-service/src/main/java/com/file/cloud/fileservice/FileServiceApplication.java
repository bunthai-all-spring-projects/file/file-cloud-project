package com.file.cloud.fileservice;

import com.file.cloud.common.CommonApplication;
import com.file.cloud.common.infrastructure.configuration.ApiConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({ApiConfiguration.class})
//@Import({ExceptionHandlerAdvice.class})
public class FileServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FileServiceApplication.class, args);
	}

}
