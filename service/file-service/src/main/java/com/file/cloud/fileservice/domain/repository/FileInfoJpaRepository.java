package com.file.cloud.fileservice.domain.repository;

import com.file.cloud.fileservice.domain.entities.FileInfo;
import com.file.cloud.fileservice.domain.entities.objectvalues.FileKey;
import com.file.cloud.fileservice.domain.entities.objectvalues.Id;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FileInfoJpaRepository extends JpaRepository<FileInfo, Long> {

    @Query(value = "select * from file_info where id=:id", nativeQuery = true)
    Optional<FileInfo> findById(long id);

    Optional<FileInfo> findByFileKey(String fileKey);

    FileInfo deleteByFileKey(FileKey fileKey);

}
