package com.file.cloud.fileservice.infrastructure.configuration;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.client.builder.AwsClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class S3ClientConfiguration {

    @Value("${amazonaws.s3.url}")
    private String ENDPOINT_URL;

    @Bean
    public AwsClientBuilder.EndpointConfiguration endpointConfiguration() {
        return new AwsClientBuilder.EndpointConfiguration(ENDPOINT_URL, null);
    }

    @Bean
    public ClientConfiguration clientConfiguration() {
        return new ClientConfiguration()
                .withConnectionTimeout(ClientConfiguration.DEFAULT_CONNECTION_TIMEOUT)
                .withSocketTimeout(ClientConfiguration.DEFAULT_SOCKET_TIMEOUT)
                .withProtocol(Protocol.HTTP);
    }
}
