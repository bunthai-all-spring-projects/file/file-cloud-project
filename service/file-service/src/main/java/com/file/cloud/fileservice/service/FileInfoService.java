package com.file.cloud.fileservice.service;

import com.file.cloud.common.exceptions.ResourceNotFoundException;
import com.file.cloud.common.exceptions.ResourceViolationException;
import com.file.cloud.fileservice.domain.entities.FileInfo;
import com.file.cloud.fileservice.domain.entities.objectvalues.FileKey;
import com.file.cloud.fileservice.domain.entities.objectvalues.FileName;
import com.file.cloud.fileservice.domain.entities.objectvalues.FileSize;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface FileInfoService {

    FileInfo insertFileInfo(FileKey fileKey, FileName fileName, FileSize fileSize) throws ResourceNotFoundException, ResourceViolationException;
    Optional<FileInfo> getFileInfoByFileKey(FileKey fileKey) throws ResourceNotFoundException;
    Page<FileInfo> getAllFileInfo(Pageable pageable);

    void deleteByFileKey(FileKey fileKey) throws ResourceNotFoundException;
}
