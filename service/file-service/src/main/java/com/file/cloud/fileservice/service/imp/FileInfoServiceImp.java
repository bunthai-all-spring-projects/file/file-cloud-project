package com.file.cloud.fileservice.service.imp;

import com.file.cloud.common.api.errorpath.ErrorPaths;
import com.file.cloud.common.api.responses.ApiError;
import com.file.cloud.common.exceptions.ResourceNotFoundException;
import com.file.cloud.common.exceptions.ResourceViolationException;
import com.file.cloud.fileservice.domain.entities.FileInfo;
import com.file.cloud.fileservice.domain.entities.objectvalues.FileKey;
import com.file.cloud.fileservice.domain.entities.objectvalues.FileName;
import com.file.cloud.fileservice.domain.entities.objectvalues.FileSize;
import com.file.cloud.fileservice.domain.repository.FileInfoJpaRepository;
import com.file.cloud.fileservice.domain.repository.FileInfoRepository;
import com.file.cloud.fileservice.service.FileInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class FileInfoServiceImp implements FileInfoService {

    @Autowired
    FileInfoJpaRepository fileInfoJpaRepository;

    @Autowired
    FileInfoRepository fileInfoRepository;

    @Override
    @Transactional
    public FileInfo insertFileInfo(FileKey fileKey, FileName fileName, FileSize fileSize) throws ResourceNotFoundException, ResourceViolationException {

        if(getFileInfoByFileKey(fileKey).isPresent())
            throw new ResourceViolationException("violation", new ApiError(HttpStatus.BAD_REQUEST, "filekey already exist", ErrorPaths.body("fileKey")));

         FileInfo fileInfo = fileInfoJpaRepository.save(
            FileInfo.builder()
                .fileKey(fileKey)
                .fileName(fileName)
                .fileSize(fileSize).build()
        );

         return getFileInfoByFileKey(fileKey).orElseThrow(() -> new ResourceNotFoundException("not found", new ApiError(HttpStatus.NOT_FOUND, "fileinfo not found",  ErrorPaths.body("fileKey"))));
    }

    @Override
    public Optional<FileInfo> getFileInfoByFileKey(FileKey fileKey) throws ResourceNotFoundException {
        return fileInfoRepository.getFileInfoByFileKey(fileKey);
    }

    @Override
    public Page<FileInfo> getAllFileInfo(Pageable pageable) {
        return fileInfoJpaRepository.findAll(pageable);
    }

    @Override
    @Transactional
    public void deleteByFileKey(FileKey fileKey) throws ResourceNotFoundException {
        fileInfoRepository.deleteFileInfoByFileKey(fileKey);
    }

}
