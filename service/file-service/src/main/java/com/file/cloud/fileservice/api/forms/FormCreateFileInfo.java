package com.file.cloud.fileservice.api.forms;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@NoArgsConstructor
public class FormCreateFileInfo {

//    @JsonProperty(required = true)
//    @NonNull
    @NotNull()
    @Pattern(regexp = "^[a-z][a-z0-9-]*$")
    @Length(min = 5, max = 255)
    private String fileKey;

    @NotNull()
    @Length(min = 1, max = 255)
    private String fileName;

    @NotNull()
    @Pattern(regexp = "^[0-9]*$")
    @Min(value = 1)
    @Max(value = 104900000)
    private String fileSize;

}
