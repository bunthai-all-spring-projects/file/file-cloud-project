package com.file.cloud.fileservice.api.controllers;

import com.file.cloud.common.api.errorpath.ErrorPaths;
import com.file.cloud.common.api.responses.*;
import com.file.cloud.common.exceptions.ResourceNotFoundException;
import com.file.cloud.common.exceptions.ResourceViolationException;
import com.file.cloud.fileservice.api.forms.FormCreateFileInfo;
import com.file.cloud.fileservice.api.view.FileInfoView;
import com.file.cloud.fileservice.domain.entities.FileInfo;
import com.file.cloud.fileservice.domain.entities.objectvalues.FileKey;
import com.file.cloud.fileservice.domain.entities.objectvalues.FileName;
import com.file.cloud.fileservice.domain.entities.objectvalues.FileSize;
import com.file.cloud.fileservice.service.FileInfoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/fileinfo")
@Slf4j
public class FileInfoController {

    @Autowired
    public FileInfoService fileInfoService;

    @PostMapping
    public ResourceResponse<FileInfoView> post(@RequestBody @Valid FormCreateFileInfo create) throws ResourceViolationException, ResourceNotFoundException {


        if (FilenameUtils.getExtension(create.getFileName()).isEmpty()) {
            throw new ResourceViolationException("violation", new ApiError(HttpStatus.BAD_REQUEST, "invalid file name", ErrorPaths.body("fileName")));
        }

        FileKey fileKey = FileKey.builder().value(create.getFileKey()).build();
        FileName fileName = FileName.builder().value(create.getFileName()).build();
        FileSize fileSize = FileSize.builder().value(Long.parseLong(create.getFileSize())).build();

        return new ResourceResponse<>(new FileInfoView(fileInfoService.insertFileInfo(fileKey, fileName, fileSize)));
    }

    @DeleteMapping("/{fileKey}")
    public NoResponse deleteByFileKey(@PathVariable("fileKey") String fileKey) throws ResourceNotFoundException {
        fileInfoService.deleteByFileKey(FileKey.builder().value(fileKey).build());
        return new NoResponse();
    }

    @GetMapping("/{fileKey}")
    public ResourceResponse<FileInfoView> getByFileKey(
            @PathVariable("fileKey") String fileKey
    ) throws ResourceNotFoundException {

        FileInfo fileInfo = fileInfoService.getFileInfoByFileKey(FileKey.builder().value(fileKey).build()).orElseThrow(() -> new ResourceNotFoundException("not found", new ApiError(HttpStatus.NOT_FOUND, "fileinfo not found", ErrorPaths.path("fileKey"))));
        return new ResourceResponse<>(new FileInfoView(fileInfo));
    }

    @GetMapping
    public ListResponse<FileInfoView> list(
        @RequestParam(value = "page", defaultValue = "0") int page,
        @RequestParam(value = "size", defaultValue = "20") int size
    ) throws ResourceViolationException {

        if (size > 200)
            throw new ResourceViolationException("violation", new ApiError(HttpStatus.BAD_REQUEST, "Page size must not be greater that 200!", ErrorPaths.query("size")));

        Pageable pageable = PageRequest.of(page, size);
        Page<FileInfo> pageFileInfo = fileInfoService.getAllFileInfo(pageable);
        List<FileInfoView> fileInfoViews = pageFileInfo.get().map(p ->
                new FileInfoView(
                        FileInfo.builder()
                                .fileKey(p.getFileKey())
                                .fileName(p.getFileName())
                                .fileSize(p.getFileSize())
                                .createdAt(p.getCreatedAt())
                                .build()
                )).collect(Collectors.toList());

        return new ListResponse<>(fileInfoViews, page, size, pageFileInfo.getNumberOfElements(), pageFileInfo.getTotalPages(), pageFileInfo.getTotalElements());
    }

}
