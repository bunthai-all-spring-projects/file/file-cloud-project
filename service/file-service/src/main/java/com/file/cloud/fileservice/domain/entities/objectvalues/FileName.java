package com.file.cloud.fileservice.domain.entities.objectvalues;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import javax.validation.constraints.NotNull;

@Value
@Builder(builderClassName = "Builder")
public class FileName {

    @NotNull
    private String value;


    @Converter(autoApply = true)
    public static class JpaConverter implements AttributeConverter<FileName, String>
    {
        @Override
        public String convertToDatabaseColumn(FileName _attribute)
        {
            return _attribute.value;
        }

        @Override
        public FileName convertToEntityAttribute(String _dbData)
        {
            return FileName.builder().value(_dbData).build();
        }
    }

}
