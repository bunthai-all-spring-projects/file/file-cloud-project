package com.file.cloud.fileservice.domain.entities.objectvalues;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

//@Value
@Getter
@AllArgsConstructor
@NoArgsConstructor
//@Getter
@Builder(builderClassName = "Builder")
@Embeddable
//@IdClass(Long.class)
public class Id implements Serializable {

//    @javax.persistence.Id
//    @SequenceGenerator(name = "file_info_seq", sequenceName = "file_info_id_seq", allocationSize = 1)
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "file_info_seq")
//    @Column(name = "id")
    private long value;

    @Converter(autoApply = true)
    public static class JpaConverter implements AttributeConverter<Id,Long>
    {
        @Override
        public Long convertToDatabaseColumn(Id _attribute)
        {
            return _attribute.value;
        }

        @Override
        public Id convertToEntityAttribute(Long _dbData)
        {
            return Id.builder().value(_dbData).build();
        }
    }

}
