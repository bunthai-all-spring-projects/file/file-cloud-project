package com.file.cloud.fileservice.domain.entities.objectvalues;

import lombok.Builder;
import lombok.Value;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import javax.validation.constraints.NotNull;

@Value
@Builder(builderClassName = "Builder")
public class FileSize {

    @NotNull
    private long value;


    @Converter(autoApply = true)
    public static class JpaConverter implements AttributeConverter<FileSize, Long>
    {
        @Override
        public Long convertToDatabaseColumn(FileSize _attribute)
        {
            return _attribute.value;
        }

        @Override
        public FileSize convertToEntityAttribute(Long _dbData)
        {
            return FileSize.builder().value(_dbData).build();
        }
    }

}
